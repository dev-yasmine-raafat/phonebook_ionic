import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public phoneRecord: any;
  // public names:any;
  public allData:any;

  public inputSearch: string;

  public inputName:string;
  public inputNumber:string;

  constructor(public alertCtrl: AlertController) {


    // this.names = Object.keys(this.phoneRecord);
  }
  ngOnInit(){
    this.initializeItems();
     //called after the constructor and called  after the first ngOnChanges()
  }
  initializeItems(){
    this.phoneRecord = [{'username':'Mohammed','number':'01011112222'},
                        {'username':'Yasmine','number':'01022223333'},
                        {'username':'Mama','number':'01033334444'},
                        {'username':'Baba','number':'01044445555'},
                        {'username':'Ahmed','number':'01055556666'}
                      ];
  }

  removeRecord(record){


    for (let contact of this.phoneRecord) {
        if (contact.username == record) {
            this.phoneRecord.splice(this.phoneRecord.indexOf(contact), 1);
            break;
        }
    }
    console.log(this.phoneRecord);
  }
  createRecord(){
    console.log(this.inputName);
    console.log(this.inputNumber);

    if(this.inputName.length >= 3 && this.inputNumber.length >= 3){
        // this.phoneRecord[this.inputName]=this.inputNumber;
        let contact = {'username':this.inputName,'number':this.inputNumber};
        this.phoneRecord.push(contact);
        this.inputName='';
        this.inputNumber='';
        console.log(this.phoneRecord);
    }else {

        this.errorAlert();

    }


  }
  updateSearchResults(searchbar){

            this.initializeItems();

    let val = searchbar.target.value;

    if(this.inputSearch == '')
      return;

      this.allData = this.phoneRecord;

    this.phoneRecord = this.phoneRecord.filter((item) => {
      return (item.username.toLowerCase().indexOf(val.toLowerCase()) > -1);
    })

  }
  async presentConfirm(name,number) {
  let alert = await this.alertCtrl.create({
    inputs: [
     {
       name: 'alertName',
       placeholder: 'Name',
       value:name
     },
     {
       name: 'alertNumber',
       placeholder: 'Number',
       value:number
     }
   ],
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Edit',
        handler: (data) => {

          console.log('Buy clicked');
          console.log(data.alertName);
          console.log(data.alertNumber);

          if(data.alertName.length >= 3 && data.alertNumber.length >= 3){
              for (var i = 0; i < this.phoneRecord.length; i++) {

                  if  (this.phoneRecord[i].username === name &&
                        this.phoneRecord[i].number === number) {

                        this.phoneRecord[i].username = data.alertName;
                        this.phoneRecord[i].number = data.alertNumber;
                  }

              }

          }else{
            this.errorAlert();
          }

        }
      }
    ]
  });
await  alert.present();
}
async errorAlert() {
  let alert = await this.alertCtrl.create({
    message: 'Contact name and number cannot be less than 3 characters length',
    buttons: ['Dismiss']
  });
  await alert.present();
}
}
